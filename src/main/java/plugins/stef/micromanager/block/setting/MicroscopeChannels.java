package plugins.stef.micromanager.block.setting;

import plugins.adufour.blocks.tools.input.InputBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.stef.micromanager.block.AbstractMicroscopeBlock;
import plugins.stef.micromanager.block.lang.VarChannels;
import plugins.stef.micromanager.block.lang.VarMMGroup;

/**
 * Define a list of channel (Micro-Manager)
 * Class in standby until an EzPlug fix
 * 
 * @author Stephane Dallongeville
 */
public class MicroscopeChannels extends AbstractMicroscopeBlock implements InputBlock
{
    VarMMGroup group;
    VarChannels channels;

    public MicroscopeChannels()
    {
        group = new VarMMGroup();
        channels = new VarChannels(group);
    }

    @Override
    public void run()
    {
        //
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        if (group != null)
            inputMap.add("groups", group);
        if (channels != null)
            inputMap.add("channels", channels);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }
}
